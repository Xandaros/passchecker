#!/usr/bin/env python3

from hashlib import sha1
from pathlib import Path
import requests
import subprocess
import sys

BLACKLIST = {".git", ".gitattributes", ".gpg-id"}
STORE = Path("/home/xandaros/.password-store")


def check_pass(path: str) -> None:
    pwd: bytes = subprocess.run(["pass", path], capture_output=True).stdout[:-1]
    hash: str = sha1(pwd).hexdigest()

    headers = {
        'User-Agent': "personal pass checker"
        }
    r = requests.get("https://api.pwnedpasswords.com/range/" + hash[:5], headers=headers)

    clean = True
    for line in r.text.splitlines():
        if line.split(":")[0].upper() == hash[5:].upper():
            clean = False
            break

    cleanstr = "\033[1;32;40mCLEAN\033[0m"
    compromisedstr = "\033[1;31;40mCOMPROMISED\033[0m"

    print(f"{path} - {compromisedstr if not clean else cleanstr}")


def check_dir(d: Path) -> None:
    for fil in d.iterdir():  # type: Path
        if fil.name in BLACKLIST:
            continue
        if fil.is_dir():
            check_dir(fil)
        else:
            check_pass(str(fil.relative_to(STORE))[:-4])


def main() -> None:
    check_dir(STORE)


if __name__ == "__main__":
    main()
